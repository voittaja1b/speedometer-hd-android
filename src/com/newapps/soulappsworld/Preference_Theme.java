package com.newapps.soulappsworld;  

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.newapps.soulappsworld.data.ConstantData;

public class Preference_Theme extends Preference 
{
	private SharedPreferences sharedPreference;
	private LinearLayout linearBlue;
	private LinearLayout linearRed;
	private Context context;

	public Preference_Theme(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		this.context=context;
	}
	public Preference_Theme(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		this.context=context;
	}
	public Preference_Theme(Context context) 
	{
		super(context);
		this.context=context;
	}

	@Override
	protected View onCreateView(ViewGroup parent) 
	{
		// this will create the linear layout defined in ads_layout.xml
		View view = super.onCreateView(parent);

		sharedPreference=context.getSharedPreferences(ConstantData.preferenceName, Context.MODE_PRIVATE);

		linearBlue=(LinearLayout)view.findViewById(R.id.linearBlueTheme);
		linearBlue.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Editor edit = sharedPreference.edit();
				edit.putBoolean(ConstantData.isBlueTheme,true);	
				edit.commit();
				Toast.makeText(context,context.getString(R.string.theme_changed_successfully), Toast.LENGTH_SHORT).show();
			}
		});

		linearRed=(LinearLayout)view.findViewById(R.id.linearRedTheme);
		linearRed.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Editor edit = sharedPreference.edit();
				edit.putBoolean(ConstantData.isBlueTheme,false);	
				edit.commit();

				Toast.makeText(context, context.getString(R.string.theme_changed_successfully), Toast.LENGTH_SHORT).show();
			}
		});

		//		// the context is a PreferenceActivity
		//		Activity activity = (Activity)getContext();
		//
		//		// Create the adView
		//		AdView adView = new AdView(activity, AdSize.BANNER, "a15135d12feb3bc");
		//		((LinearLayout)view).addView(adView);
		//
		//		// Initiate a generic request to load it with an ad
		//		AdRequest request = new AdRequest();
		//		adView.loadAd(request);     

		return view;    
	}
}

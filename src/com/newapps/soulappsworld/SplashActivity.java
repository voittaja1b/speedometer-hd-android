package com.newapps.soulappsworld;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.newapps.soulappsworld.R;
import com.newapps.soulappsworld.data.ConstantData;

public class SplashActivity extends Activity 
{	
	private int fontSize;
	private CountDownTimer counter1;
	private boolean finishFlag=false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash);
		try
		{
			Display display = getWindowManager().getDefaultDisplay(); 
			int width = display.getWidth();  // deprecated
			int height = display.getHeight();			
			ConstantData.width=width;
			ConstantData.height=height;

			Log.e("Width:- ", ""+width);
			Log.e("height:- ", ""+height);
			
			
			TextView txtAppName=(TextView)findViewById(R.id.txtAppName);
			TextView txtBy=(TextView)findViewById(R.id.txtBy);
			TextView txtCompanyName=(TextView)findViewById(R.id.txtCompanyName);
			ImageView imgIcon=(ImageView)findViewById(R.id.imgIcon);
			fontSize=20;
			if(ConstantData.width==320 && ConstantData.height==480)
			{
				fontSize=20;
			}
			else if(ConstantData.width>480)
			{
				fontSize=50;
				imgIcon.setImageResource(R.drawable.icon_500x500);
			}
			else
			{
				fontSize=30;
			}

			txtAppName.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
			txtBy.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
			txtCompanyName.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
			String language="en";

			Locale locale=Locale.getDefault();
			Log.e("Language code:- ",locale.getLanguage());

			if(locale.getLanguage().equals("ar"))
			{
				language="ar";
			}
			else if(locale.getLanguage().equals("de"))
			{
				language="de";
			}
			else if(locale.getLanguage().equals("en"))
			{
				language="en";
			}
			else if(locale.getLanguage().equals("es"))
			{
				language="es";
			}
			else if(locale.getLanguage().equals("fr"))
			{
				language="fr";
			}
			else if(locale.getLanguage().equals("he") || locale.getLanguage().equals("iw"))
			{
				language="he";
			}
			else if(locale.getLanguage().equals("it"))
			{
				language="it";
			}
			else if(locale.getLanguage().equals("ja"))
			{
				language="ja";
			}
			else if(locale.getLanguage().equals("ko"))
			{
				language="ko";
			}
			else if(locale.getLanguage().equals("nl"))
			{
				language="nl";
			}
			else if(locale.getLanguage().equals("pt"))
			{
				language="pt";
			}
			else if(locale.getLanguage().equals("ru"))
			{
				language="ru";
			}
			else if(locale.getLanguage().equals("th"))
			{
				language="th";
			}
			else if(locale.getLanguage().equals("zh_CN") || locale.getLanguage().equals("zh"))
			{
				language="zh_CN";
			}
			else if(locale.getLanguage().equals("zh_TW"))
			{
				language="zh_TW";
			}

			Log.i("Default Locale: ",language);
			Locale locale2 = new Locale(language);
			Locale.setDefault(locale2);
			Configuration config2 = new Configuration();
			config2.locale = locale2;
			getBaseContext().getResources().updateConfiguration(config2,getBaseContext().getResources().getDisplayMetrics());

		}
		catch (Exception e) 
		{
			Log.e("Getting Device ID:- Error", e.toString());
		}
		counter1 = new CountDownTimer(3000, 1000) 
		{
			@Override
			public void onTick(long millisUntilFinished) 
			{

			}

			@Override
			public void onFinish() 
			{
				if(!finishFlag)
				{
					startActivity(new Intent(SplashActivity.this,SpeedViewerActivity_Analog.class));
					finish();
				}
				finishFlag=true;
			}
		};
		counter1.start();
	}

	@Override
	protected void onDestroy() 
	{
		if(!finishFlag)
			counter1.cancel();
		finishFlag=true;
		super.onDestroy();
	}
}
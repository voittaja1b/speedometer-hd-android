package com.newapps.soulappsworld.data;

public class ConstantData 
{	
	public final static String preferenceName="PREF_SpeedoMeter_HD";	
	
	public static final String REFRESH_DATA_INTENT="RefreshData";
	public static final String speed="SPEED";
	public static final String averageKM="AVERAGE_KM";
	public static final String averageMPH="AVERAGE_MPH";
	public static final String distanceKM="DISTANCE_KM";
	public static final String distanceMPH="DISTANCE_MPH";
	public static final String maxSpeed="MAXSPEED";
	public static final String altitude="ALTITUDE";
	public static final String totalDistance="TotalDistance";
	public static final String isBlueTheme="isBlueTheme";
	
//	public static final String ="";
//	public static final String ="";
//	public static final String ="";
//	public static final String ="";
	
	public static int width;
	public static int height;
	
	
	
	
}

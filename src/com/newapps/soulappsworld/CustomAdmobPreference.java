package com.newapps.soulappsworld;  

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class CustomAdmobPreference extends Preference 
{
	public CustomAdmobPreference(Context context, AttributeSet attrs, int defStyle) {super    (context, attrs, defStyle);}
	public CustomAdmobPreference(Context context, AttributeSet attrs) {super(context, attrs);}
	public CustomAdmobPreference(Context context) {super(context);}

	@Override
	protected View onCreateView(ViewGroup parent) 
	{
		// this will create the linear layout defined in ads_layout.xml
		View view = super.onCreateView(parent);
 
		return view;    
	}
}

package com.newapps.soulappsworld;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.newapps.soulappsworld.data.ConstantData;

public class SettingsActivity extends PreferenceActivity
{
	public static boolean addShowFlag=false;

	private InterstitialAd interstitial;

	AdRequest adRequest ;
	
	private SharedPreferences sharedPreference;	
	Editor edit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference);
		
		sharedPreference=getSharedPreferences(ConstantData.preferenceName, Context.MODE_PRIVATE);
		
		
		SplashHandler mHandler = new SplashHandler();
		// set the layout for this activity

		// Create a Message object
		Message msg = new Message();
		//Assign a unique code to the message.
		//Later, this code will be used to identify the message in Handler class.
		msg.what = 0;
		// Send the message with a delay of 3 seconds(3000 = 3 sec).
		mHandler.sendMessageDelayed(msg, 8000);
		
		// Create the interstitial
	
	     adRequest = new AdRequest.Builder()
	    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
      .addTestDevice("7AF8C338E6A4EA23E303067B6C1016ED")
	    	    .build();

 

	    interstitial = new InterstitialAd(this);
	    interstitial.setAdUnitId("ca-app-pub-9366591393970813/7982363981");

	    interstitial.loadAd(adRequest);
	    
	  
		
		//Get Event on Preference
//		Preference myPref = (Preference) findPreference("proversion");
//		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() 
//		{
//			public boolean onPreferenceClick(Preference preference) 
//			{
//				Toast.makeText(SettingsActivity.this,"Pro Version Clicked",Toast.LENGTH_SHORT).show();
//				return true;
//			}
//		});
		
		Preference myPref = (Preference) findPreference("aboutus");
		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() 
		{
			public boolean onPreferenceClick(Preference preference) 
			{
				Intent intent=new Intent(SettingsActivity.this,AboutUS.class);
				startActivity(intent);
				return true;
			}
		});
		
		myPref = (Preference) findPreference("reset");
		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() 
		{
			public boolean onPreferenceClick(Preference preference) 
			{				
				AlertDialog.Builder builder = new Builder(SettingsActivity.this);
				builder.setTitle(getString(R.string.ResetDistance));
				builder.setMessage(getString(R.string.ResetConfirmation));
				builder.setPositiveButton(getString(R.string.Yes),new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						 edit = sharedPreference.edit();
						edit.putString(ConstantData.totalDistance,""+0);	
						edit.commit();

					}
				});

				builder.setNeutralButton(getString(R.string.No),new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{

					}
				});

				builder.create().show();
				return true;
			}
		});
	}

	private class SplashHandler extends Handler {

		//This method is used to handle received messages
		public void handleMessage(Message msg)
		{
			// switch to identify the message by its code
			switch (msg.what)
			{
			default:
			case 0:
				super.handleMessage(msg);



				interstitial.show();

				//Create an intent to start the new activity.
				// Our intention is to start MainActivity
				//	Intent intent = new Intent();
				//	intent.setClass(SplashScreen.this,MainList.class);
				//	startActivity(intent);
				// finish the current activity
				//	SplashScreen.this.finish();
			}
		}
	}
}

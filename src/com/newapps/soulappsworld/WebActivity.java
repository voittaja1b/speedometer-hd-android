package com.newapps.soulappsworld;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class WebActivity extends Activity
{
	public static final String URI_LINK="UriLink"; 
	private String uri;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.web);

		getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

		uri=getIntent().getStringExtra(URI_LINK);
		WebView webView=(WebView)findViewById(R.id.webview);
		
 		AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("7AF8C338E6A4EA23E303067B6C1016ED")
 	    	    .build();
     adView.loadAd(adRequest);
     
		webView.setWebChromeClient(new WebChromeClient() 
		{
			public void onProgressChanged(WebView view, int progress) 
			{
				// Activities and WebViews measure progress with different scales.
				// The progress meter will automatically disappear when we reach 100%
				setProgress(progress * 100);	
				if(progress==100)
				{
//					getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);
				}
			}
		});

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setSupportZoom(true);
		webView.loadUrl(uri);
	}
}

package com.newapps.soulappsworld;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.newapps.soulappsworld.data.ConstantData;

public class SpeedViewerActivity_Analog extends Activity
{	
	private SharedPreferences sharedPreference;

	//	private int previousUnit=1;
	private int currentUnit=1;
	private int altitudeUnit=1;
	private int fontType=2;
	private float speed_km=0;
	private float speed_miles=0;
	private double altitudeMeter=0;
	private float avg_km=0;
	private float avg_miles=0;
	private double totalDistance=0;

	private float maxSpeed_km=0;
	private float maxSpeed_miles=0;

	private Typeface normalFace;
	private Typeface digitalFace;
	private WakeLock wakeLock;
	private PowerManager pm;
	private RotateAnimation rAnim;
	private float newValue=0;
	private float oldValueKM=0;
	private float oldValueMILES=0;

//	private float speed1=0;
//	private float px=0;
//	private float py=0;
	private DataUpdateReceiver dataUpdateReceiver;
	private ImageView imgNiddle;

	/********** Started GPS Related Parameters ************/
	private LocationManager locManager;
	private boolean gps_enabled = false;
	//	private Location lastLoc=null;
	//	private double totalDistance=0;
	//	private long lastTime=0;

	private TextView txtAvg;
	private TextView txtAvgSpeed;
	private TextView txtMax;
	private TextView txtMaxSpeed;
	private TextView txtAltitude;
	private TextView txtSpeed;
	//	private TextView txtDistance;

	private TextView txtDistance1;
	private TextView txtDistance2;
	private TextView txtDistance3;
	private TextView txtDistance4;
	private TextView txtDistance5;

	private TextSwitcher switcherDistance1;
	private TextSwitcher switcherDistance2;
	private TextSwitcher switcherDistance3;
	private TextSwitcher switcherDistance4;
	private TextSwitcher switcherDistance5;

	private ImageView imgCircle;
	private SharedPreferences sp;

	private String preDist1="0";
	private String preDist2="0";
	private String preDist3="0";
	private String preDist4="0";
	private String preDist5="0";

	private int maxSpeedLimit=1;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		//TO test the Horizontal Scroll like File Manager
		//		setContentView(R.layout.top_scroll);

		//		if(ConstantData.width==320 && ConstantData.height==480)
		//			setContentView(R.layout.analog_small);
		//		else if(ConstantData.width>480)
		//			setContentView(R.layout.analog_big);
		//		else
		//			setContentView(R.layout.analog);


		if((ConstantData.width==480 && ConstantData.height==800))
			setContentView(R.layout.analog);
		else if(ConstantData.width>480)
			setContentView(R.layout.analog_big);
		else
			setContentView(R.layout.analog_small);

		sharedPreference=getSharedPreferences(ConstantData.preferenceName, Context.MODE_PRIVATE);
		
		AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("7AF8C338E6A4EA23E303067B6C1016ED")
 	    	    .build();
	     adView.loadAd(adRequest);

		
		//		Typeface face=Typeface.createFromAsset(getAssets(),"font/Ni7seg.ttf"); //Ni7seg.ttf
		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

		imgCircle=(ImageView)findViewById(R.id.imgCircle);
		sp = PreferenceManager.getDefaultSharedPreferences(SpeedViewerActivity_Analog.this);
		
		txtAvgSpeed=(TextView)findViewById(R.id.txtAvgSpeed);

		normalFace=txtAvgSpeed.getTypeface();
		digitalFace=Typeface.createFromAsset(getAssets(),"font/Ni7seg.ttf"); //Ni7seg.ttf //AndroidClock.ttf

		txtAltitude=(TextView)findViewById(R.id.txtAltitude);
		txtSpeed=(TextView)findViewById(R.id.txtSpeed);
		txtMaxSpeed=(TextView)findViewById(R.id.txtMaxSpeed);
		txtMax=(TextView)findViewById(R.id.txtMax);
		txtAvg=(TextView)findViewById(R.id.txtAvg);
		
 		
		switcherDistance1=(TextSwitcher)findViewById(R.id.switcherDistance1);
		switcherDistance2=(TextSwitcher)findViewById(R.id.switcherDistance2);
		switcherDistance3=(TextSwitcher)findViewById(R.id.switcherDistance3);
		switcherDistance4=(TextSwitcher)findViewById(R.id.switcherDistance4);
		switcherDistance5=(TextSwitcher)findViewById(R.id.switcherDistance5);

		final FrameLayout.LayoutParams lpChild=new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.FILL_PARENT);
		lpChild.gravity=Gravity.CENTER_HORIZONTAL;

		switcherDistance1.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance1 = new TextView(SpeedViewerActivity_Analog.this);
				txtDistance1.setLayoutParams(lpChild);
				txtDistance1.setGravity(Gravity.CENTER);
				txtDistance1.setText("0");

				if(ConstantData.width>480)
					txtDistance1.setTextAppearance(SpeedViewerActivity_Analog.this,android.R.style.TextAppearance_Large);
				txtDistance1.setTextColor(getResources().getColor(R.color.black));
				//				txtDistance1.setTypeface(digitalFace,Typeface.BOLD);
				//				txtDistance1.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
				//				txtDistance1.setTextSize(36);

				return txtDistance1;
			}
		});

		switcherDistance2.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance2 = new TextView(SpeedViewerActivity_Analog.this);
				txtDistance2.setLayoutParams(lpChild);
				txtDistance2.setGravity(Gravity.CENTER);
				txtDistance2.setText("0");
				
				//				txtDistance2.setTypeface(digitalFace,Typeface.BOLD);
				if(ConstantData.width>480)
					txtDistance2.setTextAppearance(SpeedViewerActivity_Analog.this,android.R.style.TextAppearance_Large);
				txtDistance2.setTextColor(getResources().getColor(R.color.black));
				return txtDistance2;
			}
		});

		switcherDistance3.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance3 = new TextView(SpeedViewerActivity_Analog.this);
				txtDistance3.setLayoutParams(lpChild);
				txtDistance3.setGravity(Gravity.CENTER);
				txtDistance3.setText("0");
				
				//				txtDistance3.setTypeface(digitalFace,Typeface.BOLD);
				if(ConstantData.width>480)
					txtDistance3.setTextAppearance(SpeedViewerActivity_Analog.this,android.R.style.TextAppearance_Large);
				txtDistance3.setTextColor(getResources().getColor(R.color.black));
				return txtDistance3;
			}
		});

		switcherDistance4.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance4 = new TextView(SpeedViewerActivity_Analog.this);
				txtDistance4.setLayoutParams(lpChild);
				txtDistance4.setGravity(Gravity.CENTER);
				txtDistance4.setText("0");
				
				//				txtDistance4.setTypeface(digitalFace,Typeface.BOLD);
				if(ConstantData.width>480)
					txtDistance4.setTextAppearance(SpeedViewerActivity_Analog.this,android.R.style.TextAppearance_Large);
				txtDistance4.setTextColor(getResources().getColor(R.color.black));
				return txtDistance4;
			}
		});

		switcherDistance5.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance5 = new TextView(SpeedViewerActivity_Analog.this);
				txtDistance5.setLayoutParams(lpChild);
				txtDistance5.setGravity(Gravity.CENTER);
				txtDistance5.setText("0");
				
				//				txtDistance5.setTypeface(digitalFace,Typeface.BOLD);
				if(ConstantData.width>480)
					txtDistance5.setTextAppearance(SpeedViewerActivity_Analog.this,android.R.style.TextAppearance_Large);
				txtDistance5.setTextColor(getResources().getColor(R.color.black));
				return txtDistance5;
			}
		});

		//Animation That Animate on Top
		Animation inTop = AnimationUtils.loadAnimation(this,R.anim.slide_in_top);
		Animation outTop = AnimationUtils.loadAnimation(this,R.anim.slide_out_top);

		switcherDistance1.setInAnimation(inTop);
		switcherDistance1.setOutAnimation(outTop);

		switcherDistance2.setInAnimation(inTop);
		switcherDistance2.setOutAnimation(outTop);

		switcherDistance3.setInAnimation(inTop);
		switcherDistance3.setOutAnimation(outTop);

		switcherDistance4.setInAnimation(inTop);
		switcherDistance4.setOutAnimation(outTop);

		switcherDistance5.setInAnimation(inTop);
		switcherDistance5.setOutAnimation(outTop);

		//		txtDistance1=new TextView(SpeedViewerActivity_Analog.this);
		//		txtDistance2=new TextView(SpeedViewerActivity_Analog.this);
		//		txtDistance3=new TextView(SpeedViewerActivity_Analog.this);
		//		txtDistance4=new TextView(SpeedViewerActivity_Analog.this);
		//		txtDistance5=new TextView(SpeedViewerActivity_Analog.this);



		//		txtDistance1=(TextView)findViewById(R.id.txtDistance1);
		//		txtDistance2=(TextView)findViewById(R.id.txtDistance2);
		//		txtDistance3=(TextView)findViewById(R.id.txtDistance3);
		//		txtDistance4=(TextView)findViewById(R.id.txtDistance4);
		//		txtDistance5=(TextView)findViewById(R.id.txtDistance5);

		imgNiddle=(ImageView)findViewById(R.id.imgNiddle);

//		int centerXOnImage=imgNiddle.getWidth()/2;
//		int centerYOnImage=imgNiddle.getHeight()/2;

//		int centerXOfImageOnScreen=imgNiddle.getLeft()+centerXOnImage;
//		int centerYOfImageOnScreen=imgNiddle.getTop()+centerYOnImage;

//		px=centerXOfImageOnScreen;
//		py=centerYOfImageOnScreen;

		ImageView imgDigital=(ImageView)findViewById(R.id.imgDigital);
		imgDigital.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent=new Intent(SpeedViewerActivity_Analog.this,SpeedViewerActivity_Digital.class);
				startActivity(intent);
			}
		});

		ImageView imgSettings=(ImageView)findViewById(R.id.imgSettings);
		imgSettings.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				//				Toast.makeText(SpeedViewerActivity_Analog.this,""+ConstantData.width+"x"+ConstantData.height,Toast.LENGTH_LONG).show();
				Intent intent=new Intent(SpeedViewerActivity_Analog.this,SettingsActivity.class);
				startActivity(intent);
			}
		});

		startService(new Intent(SpeedViewerActivity_Analog.this,LocationService.class));
		//GPS Related Calculations
		/*--------------------Started--------------------------*/
		locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		try 
		{
			gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		catch (Exception ex) 
		{
			Log.e("GPS Provider Exception", ex.toString());
		}

		// don't start listeners if no provider is enabled
		if (!gps_enabled) 
		{
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle("Attention!");
			builder.setMessage("Sorry, location is not determined. Please enable location providers");
			builder.setPositiveButton("OK",new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					if(which == DialogInterface.BUTTON_NEUTRAL)
					{
						//						Log.e("Location Info","");
						//						txtLat.setText("Sorry, location is not determined. To fix this please enable location providers");
					}
					else if (which == DialogInterface.BUTTON_POSITIVE) 
					{
						startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				}
			});

			builder.setNeutralButton("Cancel",new DialogInterface.OnClickListener() 
			{
				@Override
				public void onClick(DialogInterface dialog, int which) 
				{

				}
			});

			builder.create().show();
		}
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		if (dataUpdateReceiver == null) 
			dataUpdateReceiver = new DataUpdateReceiver();
		IntentFilter intentFilter = new IntentFilter(ConstantData.REFRESH_DATA_INTENT);
		registerReceiver(dataUpdateReceiver, intentFilter);

		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
		wakeLock.acquire();

		String dist=sharedPreference.getString(ConstantData.totalDistance,"0");
		if(dist.equalsIgnoreCase("NaN"))
		{
			totalDistance=0;	
		}	
		else
		{
			totalDistance=Double.parseDouble(dist);	
		}

		fontType = Integer.parseInt(sp.getString("font","2"));
		currentUnit=Integer.parseInt(sp.getString("speed","1"));
		maxSpeedLimit= Integer.parseInt(sp.getString("maxSpeed","1"));

		if(sharedPreference.getBoolean(ConstantData.isBlueTheme, true))
		{
			if(ConstantData.width<=480)
			{
				if(maxSpeedLimit==1)
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_digital);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_digital);
					}
				}
				else
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_max_300);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_max_300_digital);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_max_300);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_max_300_digital);
					}
				}
			}
			else
			{
				if(maxSpeedLimit==1)
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_550);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_digital_550);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_550);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_digital_550);
					}
				}
				else
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_max_550);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_max_550_digital);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_max_550);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_max_550_digital);
					}
				}
			}
		}
		else
		{
			if(ConstantData.width<=480)
			{
				if(maxSpeedLimit==1)
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_digital_);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_digital_);
					}
				}
				else
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_max_300_);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_max_300_digital_);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_max_300_);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_max_300_digital_);
					}
				}
			}
			else
			{
				if(maxSpeedLimit==1)
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_550_);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_digital_550_);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_550_);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_digital_550_);
					}
				}
				else
				{
					if(currentUnit==1) //km/h (Kilometers per hour)
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_kph_max_550_);
						else
							imgCircle.setImageResource(R.drawable.meter_kph_max_550_digital_);
					}
					else
					{
						if(fontType==1)
							imgCircle.setImageResource(R.drawable.meter_mph_max_550_);
						else
							imgCircle.setImageResource(R.drawable.meter_mph_max_550_digital_);
					}
				}
			}
		}

		if(fontType==1)
		{
			txtAvg.setTypeface(normalFace);
			txtMax.setTypeface(normalFace);
			txtAvgSpeed.setTypeface(normalFace);
			txtAltitude.setTypeface(normalFace);
			txtSpeed.setTypeface(normalFace);
			txtMaxSpeed.setTypeface(normalFace);

			((TextView)switcherDistance1.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance1.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance2.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance2.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance3.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance3.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance4.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance4.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance5.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance5.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			//			txtDistance1.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance2.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance3.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance4.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance5.setTypeface(normalFace,Typeface.BOLD);
			//			switcherDistance1.sett
		}
		else
		{
			txtAvg.setTypeface(digitalFace);
			txtMax.setTypeface(digitalFace);
			txtAvgSpeed.setTypeface(digitalFace);
			txtAltitude.setTypeface(digitalFace);
			txtSpeed.setTypeface(digitalFace);
			txtMaxSpeed.setTypeface(digitalFace);

			((TextView)switcherDistance1.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance1.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance2.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance2.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance3.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance3.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance4.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance4.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance5.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance5.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			//			txtDistance1.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance2.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance3.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance4.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance5.setTypeface(digitalFace,Typeface.BOLD);
		}

		altitudeUnit = Integer.parseInt(sp.getString("altitude","1"));
		setValues();
		//		previousUnit=currentUnit;		
	}

	//Set Values for all the Parameters Speed,Altitude etc...
	private void setValues() 
	{
		if(currentUnit==1) //km/h (Kilometers per hour)
		{
			txtSpeed.setText(String.format("%.2f kmph",speed_km));
			//			txtAvgSpeed.setText(String.format("avg: %.2f kmph",avg_km));
			//			txtMaxSpeed.setText(String.format("MaxSpeed: %.2f kmph",maxSpeed_km));
			txtAvgSpeed.setText(String.format("%.2f",avg_km));
			txtMaxSpeed.setText(String.format("%.2f",maxSpeed_km));
			newValue=speed_km;
			if(maxSpeedLimit==2)
			{
				newValue=newValue/2;	
			}
			rAnim = new RotateAnimation(oldValueKM,newValue, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
		}
		else //mph(Miles per hour)
		{
			txtSpeed.setText(String.format("%.2f mph",speed_miles));
			//			txtAvgSpeed.setText(String.format("avg: %.2f mph",avg_miles));
			//			txtMaxSpeed.setText(String.format("MaxSpeed: %.2f mph",maxSpeed_km));
			txtAvgSpeed.setText(String.format("%.2f",avg_miles));
			txtMaxSpeed.setText(String.format("%.2f",maxSpeed_miles));
			newValue=speed_miles;
			if(maxSpeedLimit==2)
			{
				newValue=newValue/2;	
			}
			rAnim = new RotateAnimation(oldValueMILES,newValue, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
		}

		if(altitudeUnit==1)
		{
			txtAltitude.setText(String.format("%.2f m",altitudeMeter));	
		}
		else
		{
			txtAltitude.setText(String.format("%.2f ft",(altitudeMeter)*3.28084));
		}
		//		totalDistance=9.12;

		String dist1="0";
		String dist2="0";
		String dist3="0";
		String dist4="0";
		String dist5="0";
		String total=""+totalDistance;

		if(totalDistance>=1000)
		{
			dist1=total.substring(0,1);
			dist2=total.substring(1,2);
			dist3=total.substring(2,3);
			dist4=total.substring(3,4);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else if(totalDistance>=100)
		{
			dist2=total.substring(0,1);
			dist3=total.substring(1,2);
			dist4=total.substring(2,3);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else if(totalDistance>10)
		{
			dist3=total.substring(0,1);
			dist4=total.substring(1,2);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else
		{
			dist4=total.substring(0,1);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}

		//		Toast.makeText(SpeedViewerActivity_Analog.this,"totalDistance: "+totalDistance+"Dist4: "+dist4+"Dist5: "+dist5,Toast.LENGTH_LONG).show();
		//txtDistance.setText(""+totalDistance);

		if(!preDist1.equals(dist1))
		{
			switcherDistance1.setText(dist1);	
		}
		if(!preDist2.equals(dist2))
		{
			switcherDistance2.setText(dist2);
		}
		if(!preDist3.equals(dist3))
		{
			switcherDistance3.setText(dist3);	
		}
		if(!preDist4.equals(dist4))
		{	
			switcherDistance4.setText(dist4);	
		}
		if(!preDist5.equals(dist5))
		{
			switcherDistance5.setText(dist5);	
		}
		preDist1=dist1;
		preDist2=dist2;
		preDist3=dist3;
		preDist4=dist4;
		preDist5=dist5;

		//		txtDistance1.setText(dist1);
		//		txtDistance2.setText(dist2);
		//		txtDistance3.setText(dist3);
		//		txtDistance4.setText(dist4);
		//		txtDistance5.setText(dist5);

		rAnim.setInterpolator(new LinearInterpolator());
		rAnim.setDuration(1000);
		rAnim.setFillAfter(true);
		rAnim.setFillEnabled(true);
		//					rAnim.setRepeatCount(Animation.INFINITE);
		//					imgNiddle.setDrawingCacheEnabled(true);
		imgNiddle.startAnimation(rAnim);

		oldValueKM=speed_km;
		oldValueMILES=speed_miles;

		//		on LG No devision here
		if(maxSpeedLimit==2) //My Device Grand(Value Devided by 2)
		{
			oldValueMILES=oldValueMILES/2;
			oldValueKM=oldValueKM/2;
		}
	}

	@Override
	protected void onPause() 
	{
		super.onPause();
		if (dataUpdateReceiver != null) 
			unregisterReceiver(dataUpdateReceiver);

		if(wakeLock.isHeld())
			wakeLock.release();
	}

	private class DataUpdateReceiver extends BroadcastReceiver 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			if (intent.getAction().equals(ConstantData.REFRESH_DATA_INTENT)) 
			{
				try
				{
					altitudeMeter=intent.getDoubleExtra(ConstantData.altitude,0.0);
					float speed_meter=intent.getFloatExtra(ConstantData.speed,0.0f);
					speed_km=(float)(speed_meter*3.6);
					speed_miles=(float)(speed_km*0.62);
					avg_km=intent.getFloatExtra(ConstantData.averageKM,0.0f);
					avg_miles=intent.getFloatExtra(ConstantData.averageMPH,0.0f);
					maxSpeed_km=(float)(intent.getFloatExtra(ConstantData.maxSpeed,0.0f)*3.6);
					maxSpeed_miles=(float)(maxSpeed_km*0.62);					
					totalDistance=intent.getDoubleExtra(ConstantData.distanceKM,0.0);

					//Round Total distance upto 2 decimal point
					totalDistance = Math.round(totalDistance * 100.0) / 100.0;

					//double roundOff = Math.round(a * 100.0) / 100.0;
					//					Output is
					//					123.14


					//					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SpeedViewerActivity_Analog.this);
					//					String speedUnit= sp.getString("speed","-1");
					//					if(speedUnit.equals("1")) //km/h (Kilometers per hour)
					//					{
					//						txtSpeed.setText("Speed:- "+String.format("%.2f km/h",speed_km));
					//						txtAvgSpeed.setText(String.format("avg: %.2f km/h",avg_km));
					//						txtMaxSpeed.setText(String.format("MaxSpeed: %.2f km/h",maxSpeed_km));
					//						newValue=speed_km;
					//						rAnim = new RotateAnimation(oldValueKM,newValue, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
					//					}
					//					else //mph(Miles per hour)
					//					{
					//						txtSpeed.setText("Speed:- "+String.format("%.2f mph",speed_miles));
					//						txtAvgSpeed.setText(String.format("avg: %.2f mph",avg_miles));
					//						txtMaxSpeed.setText(String.format("MaxSpeed: %.2f mph",maxSpeed_miles));
					//						newValue=speed_miles;
					//						rAnim = new RotateAnimation(oldValueMILES,newValue, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);
					//					}
					//					txtAltitude.setText(String.format("Altitude:- %.2f",altitudeMeter));
					//					
					//					rAnim.setInterpolator(new LinearInterpolator());
					//					rAnim.setDuration(1000);
					//					rAnim.setFillAfter(true);
					//					rAnim.setFillEnabled(true);
					//					//					rAnim.setRepeatCount(Animation.INFINITE);
					//					//					imgNiddle.setDrawingCacheEnabled(true);
					//					imgNiddle.startAnimation(rAnim);
					//					
					//					oldValueKM=speed_km;
					//					oldValueMILES=speed_miles;

					setValues();
					//					Toast.makeText(SpeedViewerActivity.this,"Speed:- "+speed,Toast.LENGTH_SHORT).show();

					//						speed1+=10;
					//						if(speed1>200)
					//						{
					//							speed1=0;
					//						}
					//						newValue=speed1;
					//						rAnim = new RotateAnimation(oldValue,newValue, Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF, 0.5F);  
					//						rAnim.setInterpolator(new LinearInterpolator());
					//						rAnim.setDuration(1000);
					//						rAnim.setFillAfter(true);
					//						rAnim.setFillEnabled(true);
					////						rAnim.setRepeatCount(Animation.INFINITE);
					////						imgNiddle.setDrawingCacheEnabled(true);
					//						imgNiddle.startAnimation(rAnim);
					//						oldValue=speed1;
				}
				catch (Exception e) 
				{
					Log.e("Receiver Exception:- ",e.toString());
				}
			}
		}
	}
}
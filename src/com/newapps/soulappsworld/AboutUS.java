package com.newapps.soulappsworld;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.newapps.soulappsworld.data.ConstantData;

public class AboutUS extends Activity
{
	private int fontSize;
	private boolean isTablet=false;

 

	private String APP_LINK="";
 	private String[] Appname={"","","","",""};
 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.aboutus);
		
		AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("7AF8C338E6A4EA23E303067B6C1016ED")
 	    	    .build();
	    adView.loadAd(adRequest);

		isTablet=isTablet(this);

		ListView list= (ListView)findViewById(R.id.lstAbout);

		fontSize=15;

		if(ConstantData.width==320 && ConstantData.height==480)
		{
			fontSize=15;
		}
		else if(ConstantData.width>480)
		{
			fontSize=15;
			if(isTablet)
			{
				fontSize=20;
			}
		}

		//		Applink = getResources().getStringArray(R.array.Applink);
		//		Appname = getResources().getStringArray(R.array.Appname);

		Appname[0]=getResources().getString(R.string.AboutUS);
		Appname[1]=getResources().getString(R.string.More_Apps);
		Appname[2]=getResources().getString(R.string.ContactUS);
		Appname[3]=getResources().getString(R.string.Share_App);
		Appname[4]=getResources().getString(R.string.Rate_This_App);
		//		Appname[3]=getResources().getString(R.string.Buy_Pro_Version);

		APP_LINK="https://play.google.com/store/apps/details?id=" + getPackageName();
		AboutAdapter aboutAdapter=new AboutAdapter();
		list.setAdapter(aboutAdapter);

		list.setOnItemClickListener(new OnItemClickListener() 
		{
			public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) 
			{
				if(position==0)
				{
					//					Intent intent = new Intent(AboutUS.this,WebActivity.class);
					//					intent.putExtra(WebActivity.URI_LINK,Applink[position]);
					//					startActivity(intent);

					Intent intent=new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse("http://www.google.com"));
					startActivity(intent);
				}
				else if(position==1)
				{
					Intent intent = new Intent(Intent.ACTION_VIEW); 
					intent.setData(Uri.parse("market://search?q=King"));
					startActivity(intent);
				}
				else if(position==2)
				{
					Intent sendIntent = new Intent(Intent.ACTION_SEND);

					//Mime type of the attachment (or) u can use sendIntent.setType("*/*")
					sendIntent.setType("text/html");

					String aEmailList[] = {"info@google.com"};
					sendIntent.putExtra(Intent.EXTRA_EMAIL,aEmailList);

					//Subject for the message or Email
					sendIntent.putExtra(Intent.EXTRA_SUBJECT,"Contact from - "+getString(R.string.app_name));

					//Body for the message or Email
					String body="";
					sendIntent.putExtra(Intent.EXTRA_TEXT ,body);

					startActivity(Intent.createChooser(sendIntent, "Send email..."));
				}
				else if(position==3)
				{
					{
						LayoutInflater li = LayoutInflater.from(AboutUS.this);
						View promptsView = li.inflate(R.layout.edit, null);

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AboutUS.this);
						// set prompts.xml to alertdialog builder
						
						alertDialogBuilder.setTitle(getString(R.string.app_name));
						alertDialogBuilder.setView(promptsView);
						
						final EditText edtText = (EditText) promptsView.findViewById(R.id.edtName);
						String body=getString(R.string.Share_App_Body_top)+" "+getString(R.string.app_name)+" "+
						getString(R.string.Share_App_Body_middle)+ " "+APP_LINK+" "+
						getString(R.string.Share_App_Body_bottom);
						edtText.setText(body);
						
						// set dialog message
						alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton(getString(R.string.Send),
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) 
							{								
								// get user input and set it to result
								// edit text
								String finalString=(edtText.getText().toString());
								
								Intent email = new Intent(Intent.ACTION_SEND);
								email.setType("text/plain");
								email.putExtra(Intent.EXTRA_EMAIL,  "" );
								email.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								email.putExtra(Intent.EXTRA_SUBJECT, " "+getString(R.string.app_name)+" "+getString(R.string.Share_App_Sub));
								
//								 
								
								email.putExtra(Intent.EXTRA_TEXT,finalString);
								
								try
								{
									startActivity(Intent.createChooser(email, "Send Message..."));
								}
								catch (android.content.ActivityNotFoundException ex) 
								{
									
								}
							}
						})
						.setNegativeButton(getString(R.string.Cancel),
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								dialog.cancel();
							}
						});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
						// show it
						alertDialog.show();
					}	
				}
				else if(position==4)
				{
					Intent intent = new Intent(Intent.ACTION_VIEW); 
					intent.setData(Uri.parse(APP_LINK));
					startActivity(intent);
				}

				//				Intent authIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Applink[position]));
				//				startActivity(authIntent);
			}
		});		
	}

	public class AboutAdapter extends BaseAdapter
	{
		private LayoutInflater layoutInflater;
		public AboutAdapter() 
		{
			layoutInflater=(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() 
		{
			return Appname.length;
		}

		@Override
		public Object getItem(int arg0) 
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) 
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) 
		{
			ViewHolder holder;
			if(convertView==null)
			{
				holder = new ViewHolder();
				if(isTablet)
				{
					convertView=layoutInflater.inflate(R.layout.row_about_tab,null);
				}
				else
					convertView=layoutInflater.inflate(R.layout.row_about,null);
				holder.txtTitle=(TextView)convertView.findViewById(R.id.txtTitle);
				//				holder.imgView=(ImageView)convertView.findViewById(R.id.imgIcon);

				convertView.setTag(holder);
			}
			else
			{
				holder=(ViewHolder) convertView.getTag();
			}

			holder.txtTitle.setText(Appname[position]);
			holder.txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,fontSize);

			//			holder.imgView.setBackgroundResource(iconList[position]);
			return convertView;
		}

		public class ViewHolder
		{
			TextView txtTitle;
			//			ImageView imgView;
		}
	}
	public boolean isTablet(Context context) 
	{
		boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
		boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
		return (xlarge || large);
	}
}


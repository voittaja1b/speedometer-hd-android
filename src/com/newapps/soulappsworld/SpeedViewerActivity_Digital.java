package com.newapps.soulappsworld;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.newapps.soulappsworld.R;
import com.newapps.soulappsworld.data.ConstantData;

public class SpeedViewerActivity_Digital extends Activity
{
	private SharedPreferences sharedPreference;

	private int currentUnit=1;
	private int fontType=2;
	private float speed_km=0;
	private float speed_miles=0;
	private float avg_km=0;
	private float avg_miles=0;
	private double totalDistance=0;

	private float maxSpeed_km=0;
	private float maxSpeed_miles=0;

	private Typeface normalFace;
	private Typeface digitalFace;
	private SharedPreferences sp;

	private WakeLock wakeLock;
	private PowerManager pm;
	private DataUpdateReceiver dataUpdateReceiver;
	private TextView txtSpeed;
	private TextView txtMaxSpeed;
	private TextView txtAvgSpeed;
	//	private TextView txtDistance;
	//	private TextView txtAltitude;

	private TextView txtDistance1;
	private TextView txtDistance2;
	private TextView txtDistance3;
	private TextView txtDistance4;
	private TextView txtDistance5;

	private TextSwitcher switcherDistance1;
	private TextSwitcher switcherDistance2;
	private TextSwitcher switcherDistance3;
	private TextSwitcher switcherDistance4;
	private TextSwitcher switcherDistance5;

	//	private TextView txtDistanceUnit;
	private TextView txtSpeedUnit;
	private TextView txtMaxSpeedUnit;
	private TextView txtAvgSpeedUnit;

	private int fontSize;
	//	private TextView txtAltitudeUnit;

	private String preDist1="0";
	private String preDist2="0";
	private String preDist3="0";
	private String preDist4="0";
	private String preDist5="0";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.digital);

		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		
		AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        .addTestDevice("7AF8C338E6A4EA23E303067B6C1016ED")
 	    	    .build();
	     adView.loadAd(adRequest);


		txtSpeed=(TextView)findViewById(R.id.txtSpeed);
		//		txtAltitude=(TextView)findViewById(R.id.txtAltitude);
		//		txtDistance=(TextView)findViewById(R.id.txtDistance);
		txtMaxSpeed=(TextView)findViewById(R.id.txtMaxSpeed);
		txtAvgSpeed=(TextView)findViewById(R.id.txtAvgSpeed);

		//		txtDistanceUnit=(TextView)findViewById(R.id.txtDistanceUnit);
		txtSpeedUnit=(TextView)findViewById(R.id.txtSpeedUnit);
		txtMaxSpeedUnit=(TextView)findViewById(R.id.txtMaxSpeedUnit);
		txtAvgSpeedUnit=(TextView)findViewById(R.id.txtAvgSpeedUnit);
		//		txtAltitudeUnit=(TextView)findViewById(R.id.txtAltitudeUnit);

		switcherDistance1=(TextSwitcher)findViewById(R.id.switcherDistance1);
		switcherDistance2=(TextSwitcher)findViewById(R.id.switcherDistance2);
		switcherDistance3=(TextSwitcher)findViewById(R.id.switcherDistance3);
		switcherDistance4=(TextSwitcher)findViewById(R.id.switcherDistance4);
		switcherDistance5=(TextSwitcher)findViewById(R.id.switcherDistance5);
		
 

		//		txtDistance1=(TextView)findViewById(R.id.txtDistance1);
		//		txtDistance2=(TextView)findViewById(R.id.txtDistance2);
		//		txtDistance3=(TextView)findViewById(R.id.txtDistance3);
		//		txtDistance4=(TextView)findViewById(R.id.txtDistance4);
		//		txtDistance5=(TextView)findViewById(R.id.txtDistance5);

		normalFace=txtAvgSpeed.getTypeface();
		digitalFace=Typeface.createFromAsset(getAssets(),"font/Ni7seg.ttf"); 

		sp = PreferenceManager.getDefaultSharedPreferences(SpeedViewerActivity_Digital.this);
		sharedPreference=getSharedPreferences(ConstantData.preferenceName, Context.MODE_PRIVATE);

		fontSize=20;
		int fontSizeSpeed=30;
		int fontSizeUnit=15;
		ImageView imgAnalog=(ImageView)findViewById(R.id.imgAnalog);
		ImageView imgSettings=(ImageView)findViewById(R.id.imgSettings);
		if(ConstantData.width==320 && ConstantData.height==480)
		{
			imgAnalog.setImageResource(R.drawable.button_analog_small);
			imgSettings.setImageResource(R.drawable.button_settings);

			fontSizeSpeed=60;
			fontSize=30;
			fontSizeUnit=20;
		}
		else if(ConstantData.width>480)
		{
			imgAnalog.setImageResource(R.drawable.button_analog_big);
			imgSettings.setImageResource(R.drawable.button_settings_big);

			fontSizeSpeed=120;
			fontSize=55;
			fontSizeUnit=30;
		}
		else
		{
			imgAnalog.setImageResource(R.drawable.button_analog);
			imgSettings.setImageResource(R.drawable.button_settings);

			fontSizeSpeed=60;
			fontSize=30;
			fontSizeUnit=20;
		}

		txtSpeed.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeSpeed);
		//		txtAltitude.setTextSize(TypedValue.COMPLEX_UNIT_SP,fontSize);
		//		txtDistance.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		txtMaxSpeed.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		txtAvgSpeed.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);

		//		txtDistanceUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeUnit);
		txtSpeedUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeUnit);
		txtMaxSpeedUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeUnit);
		txtAvgSpeedUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeUnit);
		//		txtAltitudeUnit.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeUnit);

		//		txtDistance1.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		//		txtDistance2.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		//		txtDistance3.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		//		txtDistance4.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
		//		txtDistance5.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);

		final FrameLayout.LayoutParams lpChild=new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,FrameLayout.LayoutParams.FILL_PARENT);
		lpChild.gravity=Gravity.CENTER_HORIZONTAL;

		switcherDistance1.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance1 = new TextView(SpeedViewerActivity_Digital.this);
				txtDistance1.setLayoutParams(lpChild);
				txtDistance1.setGravity(Gravity.CENTER);
				txtDistance1.setText("0");
				txtDistance1.setTextColor(getResources().getColor(R.color.black));
				txtDistance1.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);

				//				if(ConstantData.width>480)
				//					txtDistance1.setTextAppearance(SpeedViewerActivity_Digital.this,android.R.style.TextAppearance_Large);

				//				txtDistance1.setTypeface(digitalFace,Typeface.BOLD);
				//				txtDistance1.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
				//				txtDistance1.setTextSize(36);

				return txtDistance1;
			}
		});

		switcherDistance2.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance2 = new TextView(SpeedViewerActivity_Digital.this);
				txtDistance2.setLayoutParams(lpChild);
				txtDistance2.setGravity(Gravity.CENTER);
				txtDistance2.setText("0");
				txtDistance2.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
				txtDistance2.setTextColor(getResources().getColor(R.color.black));
				//				txtDistance2.setTypeface(digitalFace,Typeface.BOLD);
				//				if(ConstantData.width>480)
				//					txtDistance2.setTextAppearance(SpeedViewerActivity_Digital.this,android.R.style.TextAppearance_Large);
				return txtDistance2;
			}
		});

		switcherDistance3.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance3 = new TextView(SpeedViewerActivity_Digital.this);
				txtDistance3.setLayoutParams(lpChild);
				txtDistance3.setGravity(Gravity.CENTER);
				txtDistance3.setText("0");
				txtDistance3.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
				txtDistance3.setTextColor(getResources().getColor(R.color.black));
				//				txtDistance3.setTypeface(digitalFace,Typeface.BOLD);
				//				if(ConstantData.width>480)
				//					txtDistance3.setTextAppearance(SpeedViewerActivity_Digital.this,android.R.style.TextAppearance_Large);

				return txtDistance3;
			}
		});

		switcherDistance4.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance4 = new TextView(SpeedViewerActivity_Digital.this);
				txtDistance4.setLayoutParams(lpChild);
				txtDistance4.setGravity(Gravity.CENTER);
				txtDistance4.setText("0");
				txtDistance4.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
				txtDistance4.setTextColor(getResources().getColor(R.color.black));
				//				txtDistance4.setTypeface(digitalFace,Typeface.BOLD);
				//				if(ConstantData.width>480)
				//					txtDistance4.setTextAppearance(SpeedViewerActivity_Digital.this,android.R.style.TextAppearance_Large);

				return txtDistance4;
			}
		});

		switcherDistance5.setFactory(new ViewFactory() 
		{
			@Override
			public View makeView() 
			{
				txtDistance5 = new TextView(SpeedViewerActivity_Digital.this);
				txtDistance5.setLayoutParams(lpChild);
				txtDistance5.setGravity(Gravity.CENTER);
				txtDistance5.setText("0");
				txtDistance5.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
				txtDistance5.setTextColor(getResources().getColor(R.color.black));
				//				txtDistance5.setTypeface(digitalFace,Typeface.BOLD);
				//				if(ConstantData.width>480)
				//					txtDistance5.setTextAppearance(SpeedViewerActivity_Digital.this,android.R.style.TextAppearance_Large);

				return txtDistance5;
			}
		});

		//Animation That Animate on Top
		Animation inTop = AnimationUtils.loadAnimation(this,R.anim.slide_in_top);
		Animation outTop = AnimationUtils.loadAnimation(this,R.anim.slide_out_top);

		switcherDistance1.setInAnimation(inTop);
		switcherDistance1.setOutAnimation(outTop);

		switcherDistance2.setInAnimation(inTop);
		switcherDistance2.setOutAnimation(outTop);

		switcherDistance3.setInAnimation(inTop);
		switcherDistance3.setOutAnimation(outTop);

		switcherDistance4.setInAnimation(inTop);
		switcherDistance4.setOutAnimation(outTop);

		switcherDistance5.setInAnimation(inTop);
		switcherDistance5.setOutAnimation(outTop);

		imgAnalog.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});

		imgSettings.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				Intent intent=new Intent(SpeedViewerActivity_Digital.this,SettingsActivity.class);
				startActivity(intent);
			}
		});

		//		startService(new Intent(SpeedViewerActivity_Digital.this,LocationService.class));
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		if (dataUpdateReceiver == null) 
			dataUpdateReceiver = new DataUpdateReceiver();
		IntentFilter intentFilter = new IntentFilter(ConstantData.REFRESH_DATA_INTENT);
		registerReceiver(dataUpdateReceiver, intentFilter);

		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
		wakeLock.acquire();

		//		totalDistance=Double.parseDouble(sharedPreference.getString(ConstantData.totalDistance,"0"));
		String dist=sharedPreference.getString(ConstantData.totalDistance,"0");
		if(dist.equalsIgnoreCase("NaN"))
		{
			totalDistance=0;	
		}
		else
		{
			totalDistance=Double.parseDouble(dist);	
		}

		fontType = Integer.parseInt(sp.getString("font","2"));
		currentUnit=Integer.parseInt(sp.getString("speed","1"));

		if(fontType==1)
		{
			txtAvgSpeed.setTypeface(normalFace);
			txtSpeed.setTypeface(normalFace);
			txtMaxSpeed.setTypeface(normalFace);
			txtSpeedUnit.setTypeface(normalFace);
			txtMaxSpeedUnit.setTypeface(normalFace);
			txtAvgSpeedUnit.setTypeface(normalFace);

			//			txtDistance1.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance2.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance3.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance4.setTypeface(normalFace,Typeface.BOLD);
			//			txtDistance5.setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance1.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance1.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance2.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance2.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance3.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance3.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance4.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance4.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);

			((TextView)switcherDistance5.getChildAt(0)).setTypeface(normalFace,Typeface.BOLD);
			((TextView)switcherDistance5.getChildAt(1)).setTypeface(normalFace,Typeface.BOLD);
		}
		else
		{
			txtAvgSpeed.setTypeface(digitalFace);
			txtSpeed.setTypeface(digitalFace);
			txtMaxSpeed.setTypeface(digitalFace);
			txtSpeedUnit.setTypeface(digitalFace);
			txtMaxSpeedUnit.setTypeface(digitalFace);
			txtAvgSpeedUnit.setTypeface(digitalFace);

			//			txtDistance1.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance2.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance3.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance4.setTypeface(digitalFace,Typeface.BOLD);
			//			txtDistance5.setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance1.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance1.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance2.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance2.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance3.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance3.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance4.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance4.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);

			((TextView)switcherDistance5.getChildAt(0)).setTypeface(digitalFace,Typeface.BOLD);
			((TextView)switcherDistance5.getChildAt(1)).setTypeface(digitalFace,Typeface.BOLD);
		}

		//		altitudeUnit = Integer.parseInt(sp.getString("altitude","1"));
		setValues();
	}

	//Set Values for all the Parameters Speed,Altitude etc...
	private void setValues() 
	{
		if(currentUnit==1) //km/h (Kilometers per hour)
		{
			txtSpeed.setText(String.format("%.2f",speed_km));
			txtAvgSpeed.setText(String.format("%.2f",avg_km));
			txtMaxSpeed.setText(String.format("%.2f",maxSpeed_km));

			txtSpeedUnit.setText("kmph");
			//			txtAvgSpeedUnit.setText("kmph");
			//			txtMaxSpeedUnit.setText("kmph");
		}
		else //mph(Miles per hour)
		{
			txtSpeed.setText(String.format("%.2f",speed_miles));
			txtAvgSpeed.setText(String.format("%.2f",avg_miles));
			txtMaxSpeed.setText(String.format("%.2f",maxSpeed_miles));

			txtSpeedUnit.setText("mph");
			//			txtAvgSpeedUnit.setText("mph");
			//			txtMaxSpeedUnit.setText("mph");
		}

		//		txtDistance.setText(String.format("%.2f",totalDistance));
		//		txtDistanceUnit.setText("kmph");

		String dist1="0";
		String dist2="0";
		String dist3="0";
		String dist4="0";
		String dist5="0";
		String total=""+totalDistance;

		if(totalDistance>=1000)
		{
			dist1=total.substring(0,1);
			dist2=total.substring(1,2);
			dist3=total.substring(2,3);
			dist4=total.substring(3,4);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else if(totalDistance>=100)
		{
			dist2=total.substring(0,1);
			dist3=total.substring(1,2);
			dist4=total.substring(2,3);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else if(totalDistance>10)
		{
			dist3=total.substring(0,1);
			dist4=total.substring(1,2);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}
		else
		{
			dist4=total.substring(0,1);
			dist5=total.substring(total.indexOf('.')+1,total.indexOf('.')+2);
		}

		if(!preDist1.equals(dist1))
		{
			switcherDistance1.setText(dist1);	
		}
		if(!preDist2.equals(dist2))
		{
			switcherDistance2.setText(dist2);
		}
		if(!preDist3.equals(dist3))
		{
			switcherDistance3.setText(dist3);	
		}
		if(!preDist4.equals(dist4))
		{	
			switcherDistance4.setText(dist4);	
		}
		if(!preDist5.equals(dist5))
		{
			switcherDistance5.setText(dist5);	
		}
		preDist1=dist1;
		preDist2=dist2;
		preDist3=dist3;
		preDist4=dist4;
		preDist5=dist5;

		//		txtDistance.setText(""+totalDistance);
		//		txtDistance1.setText(dist1);
		//		txtDistance2.setText(dist2);
		//		txtDistance3.setText(dist3);
		//		txtDistance4.setText(dist4);
		//		txtDistance5.setText(dist5);
	}

	@Override
	protected void onPause() 
	{
		super.onPause();
		if (dataUpdateReceiver != null) 
			unregisterReceiver(dataUpdateReceiver);
		if(wakeLock.isHeld())
			wakeLock.release();
	}

	private class DataUpdateReceiver extends BroadcastReceiver 
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			if (intent.getAction().equals(ConstantData.REFRESH_DATA_INTENT)) 
			{
				try
				{
					//					altitudeMeter=intent.getDoubleExtra(ConstantData.altitude,0.0);
					float speed_meter=intent.getFloatExtra(ConstantData.speed,0.0f);
					speed_km=(float)(speed_meter*3.6);
					speed_miles=(float)(speed_km*0.62);
					avg_km=intent.getFloatExtra(ConstantData.averageKM,0.0f);
					avg_miles=intent.getFloatExtra(ConstantData.averageMPH,0.0f);
					maxSpeed_km=(float)(intent.getFloatExtra(ConstantData.maxSpeed,0.0f)*3.6);
					maxSpeed_miles=(float)(maxSpeed_km*0.62);					
					totalDistance=intent.getDoubleExtra(ConstantData.distanceKM,0.0);

					//Round Total distance upto 2 decimal point
					totalDistance = Math.round(totalDistance * 100.0) / 100.0;

					setValues();
					//					double altitude=intent.getDoubleExtra(ConstantData.altitude,0.0);
					//					float speed_meter=intent.getFloatExtra(ConstantData.speed,0.0f);
					//					float speed_km=(float)(speed_meter*3.6);
					//					float speed_miles=(float)(speed_km*0.62);
					//
					//					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SpeedViewerActivity_Digital.this);
					//					String speed= sp.getString("speed","-1");
					//					if(speed.equals("1")) //km/h (Kilometers per hour)
					//					{	
					//						txtSpeed.setText(String.format("Speed:- "+0.3f,speed_km)+" km/h");
					//					}
					//					else //mph(Miles per hour)
					//					{
					//						txtSpeed.setText(String.format("Speed "+0.3f,speed_miles)+" mph");
					//					}
					//					txtDistance.setText("avg: "+intent.getFloatExtra(ConstantData.averageKM,0.0f));
					//					txtAltitude.setText("Altitude "+String.format(""+0.3f,altitude));
					//					txtMaxSpeed.setText("MaxSpeed: "+intent.getFloatExtra(ConstantData.maxSpeed,0.0f));
					//					txtAvgSpeed.setText("AvgSpeed: "+intent.getFloatExtra(ConstantData.maxSpeed,0.0f));
				}
				catch (Exception e) 
				{
					Log.e("Receiver Exception:- ",e.toString());
				}
			}
		}
	}
}
package com.newapps.soulappsworld;

import java.util.ArrayList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.newapps.soulappsworld.data.ConstantData;

public class LocationService extends Service 
{
	private SharedPreferences sharedPreference;	

	private float maxSpeed=0;
	public static ArrayList<String> avgArray;

	private LocationManager locManager;
	private boolean gps_enabled = false;
	private Location lastLoc=null;
	private double totalDistance=0;
	private long lastTime=0;

	@Override
	public void onCreate() 
	{
		super.onCreate();
		//GPS Related Calculations
		/*--------------------Started--------------------------*/

		sharedPreference=getSharedPreferences(ConstantData.preferenceName, Context.MODE_PRIVATE);

		locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		try 
		{
			gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}
		catch (Exception ex) 
		{
			Log.e("GPS Provider Exception", ex.toString());
		}
		// don't start listeners if no provider is enabled
		//You can also request location updates from both the "GPS" and the "Network Location Provider" by calling requestLocationUpdates() twice�once for NETWORK_PROVIDER and once for GPS_PROVIDER.
		try
		{
			locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3,0, new MapListener());	
		}
		catch (IllegalArgumentException e) 
		{

		}
		catch (Exception e) 
		{

		}
		try
		{
			locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,3, 0, new MapListener());
		}
		catch (Exception e) 
		{

		}
		/*--------------------Finished---------------------------*/
		//GPS Related Calculations Finished
	}

	@Override
	public IBinder onBind(Intent intent) 
	{
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		avgArray=new ArrayList<String>();
		return super.onStartCommand(intent, flags, startId);
	}

	public class MapListener implements LocationListener
	{
		@Override
		public void onLocationChanged(Location location) 
		{
			if(lastLoc != null)
			{
 
				float avgKM=0;
				float sumKM=0;

				float avgMPH=0;
				float sumMPH=0;

				if(avgArray!=null)
				{
					for(int i=0;i<avgArray.size();i++)
					{
						sumKM+=Float.parseFloat(avgArray.get(i)); 			//Kilometers
						sumMPH+=(Float.parseFloat(avgArray.get(i))*0.62); 	//Miles (1 km =0.62 Miles)
					}
					if(avgArray.size()>0)
					{
						avgKM=sumKM/avgArray.size();
						avgMPH=sumMPH/avgArray.size();
					}	
				}
				else
				{
					avgArray=new ArrayList<String>();
				}


				float speed=location.getSpeed();
				//Distance in Meter
				if(lastLoc.getLatitude()!=location.getLatitude() && lastLoc.getLongitude()!=location.getLongitude());
				{
					totalDistance=Double.parseDouble(sharedPreference.getString(ConstantData.totalDistance,"0"));
					totalDistance=totalDistance*1000;

					double distance=(double)lastLoc.distanceTo(location); //Distance in Meter
					totalDistance +=distance;	
					if(distance>0)
						avgArray.add(""+speed*3.6);

					try 
					{
						Editor edit = sharedPreference.edit();
						edit.putString(ConstantData.totalDistance,""+totalDistance/1000);	
						edit.commit();
					}
					catch (Exception e) 
					{
						Log.e("Error in Store Distance",e.toString());
					}
				}
				Log.e("Total Distance",""+(totalDistance/1000));

				if(speed>maxSpeed)
					maxSpeed=speed;

				Intent intent=new Intent(ConstantData.REFRESH_DATA_INTENT);
				intent.putExtra(ConstantData.speed,speed);
				intent.putExtra(ConstantData.averageKM,avgKM);
				intent.putExtra(ConstantData.averageMPH,avgMPH);
				intent.putExtra(ConstantData.maxSpeed,maxSpeed);
				intent.putExtra(ConstantData.distanceKM,(totalDistance/1000));
				intent.putExtra(ConstantData.altitude,Math.abs(location.getAltitude()));

				sendBroadcast(intent);

				lastTime=System.currentTimeMillis();
				lastLoc = location;
			}
			else
			{
				if(location!=null)
				{
					lastTime=System.currentTimeMillis();
					lastLoc = location;	
				}
			}
		}

		@Override
		public void onProviderDisabled(String provider)
		{

		}

		@Override
		public void onProviderEnabled(String provider) 
		{

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) 
		{

		}
	}	
}
